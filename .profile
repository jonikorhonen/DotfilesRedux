#!/bin/sh
#Profile file. Runs on login.

export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$HOME/go/bin:$PATH"
export EDITOR="nvim"
#export TERMINAL="kitty"
export BROWSER="firefox"
export READER="zathura"
#export FILE="ranger"
#export SUDO_ASKPASS="$HOME/.scripts/tools/dmenupass"
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# source local profile file if exists
# Used to store local account specific settings
[ -f "$HOME/.profile_local" ] && source "$HOME/.profile_local"

if [ -n "$DESKTOP_SESSION" ];then
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR"/gcr/ssh
fi

