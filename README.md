# Joni's GNU/Linux Dotfiles

These are my dotfiles. There are many like them, but these are mine.

## Basic setup

- i3 or cinnamon
- nvim
- zsh
- ranger
- mpv
- sxiv
- zathura
- gnome-keyring
- dunst
- lightdm
- maim
- Utility scripts found under `.scripts`


## Pacman hooks

Copy needed hook files under `pacman-hooks` to `/etc/pacman.d/hooks`

- 100-systemd-boot.hook - If you use systemd-boot
- 101-dash-sh.hook - Set `/bin/sh` to real `sh`

## Host specific folders

Configs, installed applications and other things which differ from defaults. Installed applications dumped with `comm -12 <(pacman -Slq | sort) <(pacman -Qqe | sort) > pacman-list.txt` and `pacman -Qmq > aur-list.txt`

Currently added hosts:
- lappy - Huawei Matebook X Pro 2018 laptop
- colossus - Main desktop

