vim.g.mapleader = " "

local setkey = vim.keymap.set

-- Open vim explorer
-- % - create new file
-- d - create new directory
-- D - delete entry under cursor
setkey("n", "<leader>pv", vim.cmd.Ex)

-- Move highlighted selection up down
setkey("v", "J", ":m '>+1<CR>gv=gv")
setkey("v", "K", ":m '<-2<CR>gv=gv")

-- Append line below to current line with space as delimiter, keep cursor in place
setkey("n", "J", "mzJ`z")

-- Keep cursor in the middle when moving between search results
setkey("n", "n", "nzzzv")
setkey("n", "N", "Nzzzv")
-- More logical movement around wrapped lines
setkey("n", "j", "gj")
setkey("n", "k", "gk")
-- Keep cursor in the middle when jumping halfpage up/down
setkey("n", "<C-d>", "<C-d>zz")
setkey("n", "<C-u>", "<C-u>zz")

-- Move to window with <Ctrl-hjkl>
--setkey("n", "<C-h>", "<C-w>h", { desc = "Go to left window" })
--setkey("n", "<C-j>", "<C-w>j", { desc = "Go to lower window" })
--setkey("n", "<C-k>", "<C-w>k", { desc = "Go to upper window" })
--setkey("n", "<C-l>", "<C-w>l", { desc = "Go to right window" })

-- Resize window with <Ctrl-arrow keys>
setkey("n", "<C-Up>", "<cmd>resize +2<CR>", { desc = "Increase window height" })
setkey("n", "<C-Down>", "<cmd>resize -2<CR>", { desc = "Decrease window height" })
setkey("n", "<C-Left>", "<cmd>vertical resize -2<CR>", { desc = "Decrease window width" })
setkey("n", "<C-Right>", "<cmd>vertical resize +2<CR>", { desc = "Increase window width" })

-- Keep current yanked selection when copying over new selection
setkey("x", "<leader>p", [["_dP]])
-- Delete selection
setkey({"n", "v"}, "<leader>d", [["_d]])
-- Yank to system clipboard
setkey({"n", "v"}, "<leader>y", [["+y]])
-- Yank current row to system clipboard
setkey("n", "<leader>Y", [["+Y]])

setkey("n", "Q", "<nop>")
setkey("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")

-- Replace all instances of word under cursor in file
setkey("n", "<leader>s", [[:%s/<C-r><C-w>/<C-r><C-w>/gI<Left><Left><Left>]])
-- Replace all instances of visual selection in file
setkey("v", "<leader>s", "y:%s/<C-r>0/<C-r>0/gI<Left><Left><Left>")
-- Make current file executable
setkey("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

setkey("n", "<leader>f", vim.lsp.buf.format)

-- GO helpers
setkey(
    "n",
    "<leader>ee",
    "oif err != nil {<CR>}<Esc>Oreturn err<Esc>"
)

-- TODO: comfy keymap for previous/next buffer
--setkey("n", "something good here", ":bprev")
--,setkey("n", "something good here", ":bnext")
