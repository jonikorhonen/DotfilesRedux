local function go_mod_tidy()
    vim.cmd([[ ! go mod tidy ]])
    vim.cmd([[ LspRestart ]])
end
vim.api.nvim_create_user_command('GoModTidy', go_mod_tidy, {})
