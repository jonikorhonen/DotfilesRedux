local lsp = require('lsp-zero').preset({})
local lspconfig = require('lspconfig')

lsp.preset('recommended')

lsp.ensure_installed({
    'tsserver',
    --'eslint',
    --'sumneko_lua',
    'rust_analyzer',
})

lspconfig.lua_ls.setup(lsp.nvim_lua_ls())
--lsp.configure('lua-language-server', {
--    settings = {
--        Lua = {
--            diagnostics = {
--                globals = { 'vim' }
--            },
--            telemetry = {
--                enable = false
--            }
--        }
--    }
--})

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

vim.filetype.add({ extension = { templ = "templ" } })
require('lspconfig.configs').templ = {
    default_config = {
        cmd = { "templ", "lsp", "-http=localhost:7474", "-log=/home/joni/.local/state/nvim/templ.log" },
        filetypes = { "templ" },
        root_dir = lspconfig.util.root_pattern("go.mod", ".git"),
        settings = {}
    }
}
lspconfig.templ.setup ({
    capabilities = capabilities,
})

lspconfig.html.setup({
    capabilities = capabilities,
    filetypes = { "html", "templ" },
})

local cmp = require('cmp')
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-y>'] = cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Replace }),
})

lsp.setup_nvim_cmp({
    mapping = cmp_mappings
})

lsp.set_preferences({
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})

---@diagnostic disable-next-line: unused-local
lsp.on_attach(function(client, bufnr)
    local opts = {buffer = bufnr, remap = false}

    local map_keys = function (mode, keys, func, desc)
        if desc then
            desc = 'LSP: ' .. desc
        end

        vim.keymap.set(mode, keys, func, { buffer = bufnr, remap = false, desc = desc })
    end
    local nmap = function (keys, func, desc)
        map_keys('n', keys, func, desc)
    end
    local imap = function (keys, func, desc)
        map_keys('i', keys, func, desc)
    end



    nmap('gd', require('telescope.builtin').lsp_definitions, 'Goto definitions')
    nmap('gr', require('telescope.builtin').lsp_references, 'Goto references')
    nmap('gI', require('telescope.builtin').lsp_implementations, 'Goto implementations')
    nmap('<leader>D', require('telescope.builtin').lsp_type_definitions, 'Goto type definitions')
    nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, 'Document symbols')

    nmap('<leader>vd', vim.diagnostic.open_float, 'Show diagnostic in float')

    nmap('K', vim.lsp.buf.hover, 'Hover documentation')
    nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature documentation')
    imap('<C-k>', vim.lsp.buf.signature_help, 'Signature documentation')

    vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
    vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)

    -- Rarer to be used
    nmap('gD', vim.lsp.buf.declaration, 'Goto declaration')

    vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
        vim.lsp.buf.format()
    end, { desc = 'Format current buffer with LSP' })
end)

-- Setup in dedicated extension
lsp.skip_server_setup({'jdtls'})

lsp.setup()
