local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>pf', builtin.find_files, { desc = '[pf] Fuzzy find files' })
vim.keymap.set('n', '<C-p>', function()
    builtin.git_files({ show_untracked = true })
end, { desc = '[C-p] Fuzzy find from git tracked files' })
-- Requires `ripgrep`
vim.keymap.set('n', '<leader>ps', function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") })
end, { desc = '[ps] Search by grep' })
vim.keymap.set('n', '<leader>/', function ()
    builtin.current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
        --winblend = 10,
        previewer = false,
    })
end, { desc = '[/] Fuzzy find in current buffer' })
vim.keymap.set('n', '<leader>pw', builtin.grep_string, { desc = '[pw] Search current word' })
